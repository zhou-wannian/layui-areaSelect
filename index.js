layui.config({
    base: './components/'
});

layui.use(["area","jquery"],function(){
    var area = layui.area;
    var $ = layui.jquery;
    
    $("#sltBtn").click(function(){
            
        area.select({
            title:"请选择指定区域",
            url:"components/area_data.json",
            //selected:{countries:[],provinces:[],cities:[],counties:[]},//选中回显
            //expSelected:{countries:[],provinces:[],cities:[],counties:[]},//需要排除已经选中的
            level:3,//指定层级(1-省级、2-市级；3-区县)
            selected:{countries:[],provinces:["150000","410000"],cities:["130100","430100"],counties:[]},//选中回显
            expSelected:{countries:[],provinces:["110000"],cities:["220100"],counties:[]},//需要排除已经选中的
            callback:function(rlt){
                console.log(rlt);
                $("#areaCodes").val(JSON.stringify(rlt.areaCodes));
                $("#areaNames").val(rlt.areaNames);
            }
        });
        
    });
})